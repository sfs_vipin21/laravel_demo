<?php

namespace App\Listeners;

use App\Events\LoginHistory;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class StoreUserLoginHistory
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoginHistory  $event
     * @return void
     */
    public function handle(LoginHistory $event)
    {
        $current_time = Carbon::now()->toDateTimeString();

        $userinfo = $event->user;

        $result = DB::table('login_history')->insert(
            [
                'name'=> $userinfo->name,
                'email'=> $userinfo->email,
                'created_at'=> $current_time,
                'updated_at'=> $current_time
            ]
        );

        return $result;
    }
}
