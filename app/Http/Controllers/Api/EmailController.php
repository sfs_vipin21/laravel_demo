<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Jobs\SendEmailJob;


class EmailController extends Controller
{
    public function sendEmail()
    {
        dispatch(new SendEmailJob());
        echo 'email sent';
    }
}
