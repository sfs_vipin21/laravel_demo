<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        
        if(\Auth::user())
        {
            $user = User::all();
            return response(['users'=> $user, 'status'=>200]);
        }
        else
        {
            return response(['message'=> 'You are not logged in', 'status'=>201]);
        }
    
    }

    public function store(Request $request)
    {
        if(auth()->user()->is_admin)
        {
            
            $validatedData = $request->validate([
                'name' => 'required|max:55',
                'email' => 'email|required|unique:users',
                'password' => 'required'
            ]);

            $validatedData['password'] = bcrypt($request->password);
    
            $user = User::create($validatedData);

            // $accessToken = $user->createToken('authToken')->accessToken;

            return response(['success'=> 'User Created Successfully', 'status'=>200]);
        }
        else
        {
            return response(['failed'=>'You do not have access to create user.', 'status'=>201]);
        }
        
    }

    public function update(Request $request, $id)
    {
        if(auth()->user()->is_admin)
        {
            $user = User::find($id);

            $request['password'] = bcrypt($request->password);

            $user->update($request->all());

            return response(['success'=> 'User Updated Successfully', 'status'=>200]);
        }
        else
        {
            return response(['failed'=>'You have no access to update user', 'status'=>201]);
        }
    }

    public function destroy($id)
    {
        if(auth()->user()->is_admin)
        {
            $user = User::find($id);
            $user->delete();

            return response(['success'=>'User Deleted Successfully', 'status'=>200]);
        }
        else
        {
            return response(['success'=>'You have no access to delete any user', 'status'=>201]);
        }
    }

    public function makeAdmin($id)
    {
        if(auth()->user()->is_admin)
        {
            $user = User::find($id);

            $user->is_admin = 1;

            $user->update();

            return response(['user'=>$user, 'success'=> 'User is admin now', 'status'=>200]);

        }
    }

    public function userPosts($id)
    {
        $user = User::find($id);

        $user->posts;

        return response([
            'user' => $user,
            'status'=> 200
        ]);
    }
}
