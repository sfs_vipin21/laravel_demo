<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

//Queue Job
Route::get('send-mail', [EmailController::class, 'sendEmail']);

Route::middleware('auth:api')->group(function(){

    Route::get('logout', [AuthController::class, 'logout']);

    Route::get('users', [UserController::class, 'index']);
    Route::post('users/create', [UserController::class, 'store']);
    Route::put('users/update/{id}', [UserController::class, 'update']);
    Route::delete('users/delete/{id}', [UserController::class, 'destroy']);
    Route::put('users/makeAdmin/{id}', [UserController::class, 'makeAdmin']);
    Route::get('users/userPosts/{id}', [UserController::class, 'userPosts']);

    Route::resource('posts', PostController::class);



});
